<?php

/**
 * @file
 * Custom theme hooks.
 */

/**
 * Preprocess hook for 'image' template.
 */
function pangea_preprocess_image(&$vars) {
  // If there is no 'title' set, then use the 'alt' text.
  if (empty($vars['attributes']['title']) && !empty($vars['attributes']['alt'])) {
    $vars['attributes']['title'] = $vars['attributes']['alt'];
  }
}

