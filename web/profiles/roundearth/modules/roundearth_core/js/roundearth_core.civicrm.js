/**
 * @file
 * Custom scripts for civicrm menu.
 */
(function (Drupal, $) {

  Drupal.behaviors.civicrmMenu = {
    attach: function (context) {
      // Add a custom class to body if the civicrm menu is loaded.
      let $civicrm = $('#civicrm-menu');
      $civicrm.ready(function () {
        if ($civicrm.length) {
          $('body').addClass('has-civicrm-menu');

          $('#crm-qsearch').focusin(function () {
            $(this).addClass('focus');
          });
          $('#crm-qsearch').focusout(function () {
            $(this).removeClass('focus');
          });

          $('#sort_name_navigation').change(function () {
            $(this).parent().toggleClass('has-value', this.value.trim() !== '');
          });
        }
      });
    }
  };

  // Overrides height computation to accommodate CiviCRM toolbar.
  Drupal.toolbar.ToolbarVisualView = Drupal.toolbar.ToolbarVisualView.extend({
    updateToolbarHeight: function updateToolbarHeight() {
      var toolbarTabOuterHeight = $('#toolbar-bar').find('.toolbar-tab').outerHeight() || 0;
      var toolbarTrayHorizontalOuterHeight = $('.is-active.toolbar-tray-horizontal').outerHeight() || 0;
      var height = toolbarTabOuterHeight + toolbarTrayHorizontalOuterHeight;

      var civiMenuHeight = $('#civicrm-menu > li').outerHeight() || 0;
      height = height + civiMenuHeight;
      this.model.set('height', height);

      $('body').each(function () {
        this.style.setProperty('padding-top', height + 'px', 'important');
      });

      this.triggerDisplace();
    }
  });

})(Drupal, jQuery);
