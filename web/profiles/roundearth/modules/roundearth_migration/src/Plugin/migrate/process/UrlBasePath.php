<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use DOMDocument;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UrlBasePath.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_url_base_path"
 * )
 */
class UrlBasePath extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Default configuration values.
   *
   * @var array
   */
  protected $defaultConfig = [
    'base_path' => '/',
    'files_path' => 'sites/default/files',
    'hosts' => [],
    'source_base_path_map' => []
  ];

  /**
   * The migration, or NULL if not available.
   *
   * @var \Drupal\migrate\MigrateExecutableInterface|null
   */
  protected $migrateExecutable;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * UrlBasePath constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $settings) {
    $this->settings = $settings;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('roundearth_migration.settings')
    );
  }

  /**
   * Get plugin configuration.
   *
   * @return array
   *   Configuration values.
   */
  protected function getConfig() {
    $config = [];
    foreach ($this->defaultConfig as $key => $value) {
      $val = $this->settings->get($key);
      $config[$key] = $val !== NULL ? $val : $this->defaultConfig[$key];
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $this->migrateExecutable = $migrate_executable;
    return $this->processUrls($value, $row);
  }

  /**
   * @param string $text
   * @return string
   */
  protected function processUrls($text, Row $row) {
    $document = Html::load($text);
    $updated = FALSE;

    // Find links.
    /** @var \DOMElement[] $links */
    $links = iterator_to_array($document->getElementsByTagName('a'));
    foreach ($links as $link) {
      $original = $link->getAttribute('href');
      if ($url = $this->transformUrl($original)) {
        $link->setAttribute('href', $url);
        $message = sprintf('Link with href="%s" updated to "%s".', $original, $url);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        $updated = TRUE;
      }
      else {
        $message = sprintf('Link with href="%s" not updated.', $original);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
      }
    }

    // Find images.
    /** @var \DOMElement[] $images */
    $images = iterator_to_array($document->getElementsByTagName('img'));
    foreach ($images as $image) {
      $original = $image->getAttribute('src');
      if ($url = $this->transformUrl($original)) {
        $image->setAttribute('src', $url);
        $message = sprintf('Image with src="%s" updated to "%s".', $original, $url);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        $updated = TRUE;
      }
      else {
        $message = sprintf('Image with src="%s" not updated.', $original);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
      }
    }

    // If we've not updated anything, return the unaltered text.
    return $updated ? Html::serialize($document) : $text;
  }

  /**
   * @param string $url
   * @return string|null
   */
  protected function transformUrl($url) {
    $url = trim($url);
    $conf = $this->getConfig();
    $parts = parse_url($url);

    // If there's a host, make sure it's the right one.
    if (!empty($parts['host'])) {
      if (!in_array($parts['host'], $conf['hosts'])) {
        return NULL;
      }
    }

    // If it's just a fragment, don't process it.
    if (substr($url, 0, 1) == '#') {
      return NULL;
    }

    // Ensure the path starts with a slash.
    $url = $parts['path'];
    if (substr($url, 0, 1) != '/') {
      $url = '/' . $url;
    }

    // Add the query and fragment.
    if (!empty($parts['query'])) {
      $url .= '?' . $parts['query'];
    }

    if (!empty($parts['fragment'])) {
      $url .= '#' . $parts['fragment'];
    }

    // Replace some stuff!
    foreach ($conf['source_base_path_map'] as $src => $dest) {
      $src = $this->normalizeSlashes($src);
      $dest = $this->normalizeSlashes($dest);
      $srcLen = strlen($src);

      if (substr($url, 0, $srcLen) == $src) {
        $url = $dest . substr($url, $srcLen);
      }
    }

    return $url;
  }

  /**
   * @param string $string
   * @return string
   */
  protected function normalizeSlashes($string) {
    $string = trim(trim($string), '/');
    $string = '/' . $string;
    if (strlen($string) > 1) {
      $string .= '/';
    }
    return $string;
  }

  /**
   * Logs a message on the current migration.
   *
   * @param string $message
   *   The message.
   * @param int $level
   *   The message level, defaults to ERROR.
   */
  protected function log($message, $level = MigrationInterface::MESSAGE_ERROR) {
    if ($this->migrateExecutable) {
      $this->migrateExecutable->saveMessage($message, $level);
    }
  }

}
