<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TextFormatMapping.
 *
 * Hardcoded mapping of text formats, for ease and consistency when configuring
 * migrations.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_text_format"
 * )
 */
class TextFormatMapping extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Format mappings.
   *
   * @var array
   */
  protected $formatMap = [];

  /**
   * Constructs a TextFormatMapping.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param array $format_map
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $format_map) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if ($format_map) {
      $this->formatMap = $format_map;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('roundearth_migration.settings')->get('text_formats')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($this->formatMap[$value])) {
      throw new MigrateException(sprintf('Invalid text format "%s" specified.', $value));
    }

    return $this->formatMap[$value];
  }

}
