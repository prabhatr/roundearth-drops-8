<?php

/**
 * @file
 * Install, update and uninstall functions for the Round Earth installation profile.
 */

use Drupal\Core\Config\ExtensionInstallStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\shortcut\Entity\Shortcut;

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function roundearth_install() {
  $config_factory = \Drupal::configFactory();

  // Temporarily use '/node' as the front page - this is needed or
  // 'civicrmtheme' will crash on the front page because there is no route.
  $config_factory->getEditable('system.site')->set('page.front', '/node')->save(TRUE);

  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // We install some menu links, so we have to rebuild the router, to ensure the
  // menu links are valid.
  \Drupal::service('router.builder')->rebuildIfNeeded();

  // Allow authenticated users to use shortcuts.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access shortcuts']);

  // Populate the default shortcut set.
  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('Add content'),
    'weight' => -20,
    'link' => ['uri' => 'internal:/node/add'],
  ]);
  $shortcut->save();

  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('All content'),
    'weight' => -19,
    'link' => ['uri' => 'internal:/admin/content'],
  ]);
  $shortcut->save();

  // Allow all users to use search.
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['search content']);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['search content']);

  // Remove unnecessary default image styles (from 'image' module).
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
  $entity_type_manager = \Drupal::service('entity_type.manager');
  $image_style_storage = $entity_type_manager->getStorage('image_style');
  // @todo Core's Media module requires the thumbnail and medium style.
  $image_styles = $image_style_storage->loadMultiple(['large']);
  $image_style_storage->delete($image_styles);

  // Some default settings for CiviCRM.
  db_query("INSERT INTO civicrm_setting (name, value, domain_id, contact_id, is_domain) VALUES ('userFrameworkResourceURL', :value, 1, 1, 1)",
    array(':value' => serialize('[cms.root]/libraries/civicrm/')));

  // Install Mosaico extension.
  _roundearth_install_civicrm_extension([
    'org.civicrm.shoreditch',
    'org.civicrm.flexmailer',
    'uk.co.vedaconsulting.mosaico',
  ]);

  // Get the active theme.
  $theme = \Drupal::service('theme.manager')->getActiveTheme()->getName();

  // Get the site name and set the default copyright.
  $site_name = $config_factory->get('system.site')->get('name');
  $config_factory->getEditable($theme . '.settings')
    ->set('copyright', '© [YEAR] ' . $site_name)
    ->save(TRUE);
}

/**
 * Install a CiviCRM extension.
 */
function _roundearth_install_civicrm_extension($extensions) {
  if (!is_array($extensions)) {
    $extensions = [ $extensions ];
  }

  \Drupal::service('civicrm')->initialize();

  civicrm_api('extension', 'refresh', array('version' => 3));

  foreach ($extensions as $extension) {
    $result = civicrm_api('extension', 'install', array('key' => $extension, 'version' => 3));
    if($result['values'] != 1) {
      return FALSE;
    }
  }

  return TRUE;
}

/**
 * Implements hook_update_dependencies().
 */
function roundearth_update_dependencies() {
  $dependencies = [];
  $dependencies['panopoly_wysiwyg'][8202] = [
    'roundearth' => 8004,
  ];
  $dependencies['roundearth'][8004] = [
    'system' => 8400,
  ];
  return $dependencies;
}

/**
 * Replace social links block with the branding block in the header_left region.
 */
function roundearth_update_8001() {
  $config_factory = \Drupal::configFactory();

  // Delete social links block config.
  $config_factory->getEditable('block.block.pangea_sociallinks')
    ->delete();

  // Update site branding block config.
  $config_factory->getEditable('block.block.pangea_sitebranding')
    ->set('region', 'header_left')
    ->set('settings.use_site_name', FALSE)
    ->set('status', TRUE)
    ->save(TRUE);
}

/**
 * Enable civicrm_cron module.
 */
function roundearth_update_8002() {
  \Drupal::service('module_installer')->install(['civicrm_cron']);
}

/**
 * Enable the menu block module.
 */
function roundearth_update_8003() {
  \Drupal::service('module_installer')->install(['menu_block']);
}

/**
 * Import the thumbnail and medium image style, again, for Panopoly Media.
 */
function roundearth_update_8004() {
  $config = [
    'image.style.medium',
    'image.style.thumbnail',
  ];

  $entity_type_manager = \Drupal::entityTypeManager();
  $active_config_storage = \Drupal::getContainer()->get('config.storage');
  $extension_config_storage = new ExtensionInstallStorage($active_config_storage, InstallStorage::CONFIG_INSTALL_DIRECTORY);

  foreach ($config as $config_name) {
    $config_data = $extension_config_storage->read($config_name);
    if ($active_config_storage->read($config_name)) {
      // Exists.
      continue;
    }
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $entity_storage */
    $entity_storage = $entity_type_manager->getStorage('image_style');
    $entity = $entity_storage->createFromStorageRecord($config_data);
    $entity->save();
  }

  \Drupal::service('module_installer')->install(['panopoly_media']);
}

/**
 * Install Mosaico extension.
 */
function roundearth_update_8005() {
  _roundearth_install_civicrm_extension([
    'org.civicrm.shoreditch',
    'org.civicrm.flexmailer',
    'uk.co.vedaconsulting.mosaico',
  ]);
}

/**
 * Revert the Core, Pages, News and Widgets features to get new fields.
 */
function roundearth_update_8006() {
  $features_installed = \Drupal::moduleHandler()->moduleExists('features');
  if (!$features_installed) {
    \Drupal::service('module_installer')->install(['features']);
  }

  /** @var \Drupal\Core\Field\FormatterPluginManager $formatter_plugin_manager */
  $formatter_plugin_manager = \Drupal::service('plugin.manager.field.formatter');
  // Clear out field formatter plugin definitions to get the new one in
  // panopoly_media, so that the config will import.
  $formatter_plugin_manager->clearCachedDefinitions();

  $feature_names = [
    'panopoly_media',
    'roundearth_core',
    'roundearth_pages',
    'roundearth_news',
    'roundearth_widgets',
  ];

  /** @var \Drupal\features\FeaturesManagerInterface $manager */
  $manager = \Drupal::service('features.manager');

  foreach ($feature_names as $feature_name) {
    $feature = $manager->loadPackage($feature_name, TRUE);

    // Get all config (even if not overridden).
    $components = $feature->getConfigOrig();

    // Revert it.
    $config_to_create = array_fill_keys($components, '');
    $manager->createConfiguration($config_to_create);
  }

  if (!$features_installed) {
    \Drupal::service('module_installer')->uninstall(['features']);
  }
}

/**
 * Switch to our custom sub-theme of Seven.
 */
function roundearth_update_8007() {
  // Enable new theme.
  \Drupal::service('theme_installer')->install(['seven_roundearth']);

  //
  // Configure block placements for the theme
  //

  $entity_type_manager = \Drupal::entityTypeManager();
  $active_config_storage = \Drupal::getContainer()->get('config.storage');
  $extension_config_storage = new ExtensionInstallStorage($active_config_storage, InstallStorage::CONFIG_INSTALL_DIRECTORY);
  $entity_storage = $entity_type_manager->getStorage('block');

  // Delete the old block placements (core copies them from the public theme).
  $old_block_placement_ids = \Drupal::entityQuery('block')
    ->condition('theme', 'seven_roundearth')
    ->execute();
  print_r($ids);
  $old_block_placements = $entity_storage->loadMultiple($old_block_placement_ids);
  $entity_storage->delete($old_block_placements);

  $config = [
    'block.block.seven_roundearth_breadcrumbs',
    'block.block.seven_roundearth_content',
    'block.block.seven_roundearth_help',
    'block.block.seven_roundearth_local_actions',
    'block.block.seven_roundearth_login',
    'block.block.seven_roundearth_messages',
    'block.block.seven_roundearth_page_title',
    'block.block.seven_roundearth_primary_local_tasks',
    'block.block.seven_roundearth_secondary_local_tasks',
  ];

  foreach ($config as $config_name) {
    $config_data = $extension_config_storage->read($config_name);
    if ($active_config_storage->read($config_name)) {
      // Exists.
      continue;
    }
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $entity_storage */
    $entity = $entity_storage->createFromStorageRecord($config_data);
    $entity->save();
  }

  // Change the Drupal admin theme.
  \Drupal::configFactory()->getEditable('system.theme')
    ->set('admin', 'seven_roundearth')
    ->save(TRUE);

  // Change CiviCRM to use the new admin theme.
  \Drupal::configFactory()->getEditable('civicrmtheme.settings')
    ->set('admin_theme', 'seven_roundearth')
    ->save(TRUE);
}

/**
 * Enable modules for admin toolbar, block content and menu permissions.
 */
function roundearth_update_8008() {
  \Drupal::service('module_installer')->install([
    'admin_toolbar',
    'admin_toolbar_links_access_filter',
    'block_content_permissions',
    'menu_admin_per_menu',
  ]);
}

/**
 * Permission fixes for authenticated users and managers.
 */
function roundearth_update_8009() {
  // Allow authenticated users to view and edit their profiles.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, [
    'profile edit',
    'profile view',
  ]);

  // Allow managers to access...
  user_role_grant_permissions('manager', [
    // Basic site information
    'administer system site information settings',

    // Blocks
    'administer blocks',
    'access administration pages',
    'view restricted block content',

    // Add/edit links on certain menus
    'administer footer menu items',
    'administer header menu items',
    'administer main menu items',
    'administer social-links menu items',
    'administer account menu items',
  ]);
}

/**
 * Set the default copyright for the site.
 */
function roundearth_update_8010() {
  $config_factory = \Drupal::configFactory();

  // Get the active theme.
  $theme = \Drupal::service('theme.manager')->getActiveTheme()->getName();

  // Get the site name and set the default copyright.
  $site_name = $config_factory->get('system.site')->get('name');
  $config_factory->getEditable($theme . '.settings')
    ->set('copyright', '© [YEAR] ' . $site_name)
    ->save(TRUE);
}

/**
 * Enable YouTube video embed privacy mode.
 */
function roundearth_update_8011() {
  \Drupal::configFactory()
    ->getEditable('video_embed_field.settings')
    ->set('privacy_mode', TRUE)
    ->save(TRUE);
}
