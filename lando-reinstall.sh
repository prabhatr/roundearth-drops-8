#!/bin/bash

#set -e

echo "SET FOREIGN_KEY_CHECKS=0; DROP DATABASE pantheon; CREATE DATABASE pantheon;" | lando mysql
rm -rf web/sites/default/files/*
chmod +w web/sites/default

exec ./lando-install.sh

